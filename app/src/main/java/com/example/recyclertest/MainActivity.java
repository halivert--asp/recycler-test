package com.example.recyclertest;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.OnItemActivatedListener;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StableIdKeyProvider;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;

public class MainActivity extends AppCompatActivity {
    private final MyAdapter ad = new MyAdapter(listado());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView rv = findViewById(R.id.recycler_view);
        rv.setHasFixedSize(true);

        RecyclerView.LayoutManager lm = new LinearLayoutManager(this);
        rv.setLayoutManager(lm);
        rv.setAdapter(ad);

        SelectionTracker tracker = new SelectionTracker.Builder<>(
                "id",
                rv,
                new StableIdKeyProvider(rv),
                new MyItemDetailsLookup(rv),
                StorageStrategy.createLongStorage()).
                withOnItemActivatedListener(new OnItemActivatedListener<Long>() {
                    @Override
                    public boolean onItemActivated(
                            @NonNull ItemDetailsLookup.ItemDetails<Long> item,
                            @NonNull MotionEvent e
                    ) {
                        Toast.makeText(
                                getApplicationContext(),
                                String.valueOf(item.getPosition()),
                                Toast.LENGTH_LONG
                        ).show();

                        try {
                            Palabra[] palabras = ad.getPalabras();
                            int position = item.getPosition();
                            Log.e("Error", palabras[position].toJSON().toString());
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        return true;
                    }
                }).build();
    }

    public Palabra[] listado() {
        Palabra[] listado = new Palabra[]{
                new Palabra(
                        1,
                        "política",
                        "Ciencia que trata del gobierno y la organización de las " +
                                "sociedades humanas, especialmente de los estados.",
                        "Del griego polis=ciudad",
                        "No Existe",
                        "No Existe")
                ,
                new Palabra(
                        2,
                        "constitución",
                        "Es un texto jurídico-político, surgido de un poder " +
                                "constituyente, que tiene el propósito de constituir la " +
                                "separación de poderes, definiendo y creando los poderes " +
                                "constituidos (legislativo, ejecutivo y judicial).",
                        "Del latin constitutio=establecer",
                        "Carta Magna",
                        "No Existe"
                ),
                new Palabra(
                        3,
                        "civismo",
                        "Comportamiento de la persona que cumple con sus deberes de " +
                                "ciudadano y respeta las leyes.",
                        "Del frances civisme=ciudad",
                        "No existe",
                        "incivismo"
                ),
                new Palabra(
                        4,
                        "ética",
                        "Disciplina filosófica que estudia el bien y el mal y sus " +
                                "relaciones con la moral y el comportamiento humano.",
                        "Del griego ethos=hábito",
                        "moralidad",
                        "antiético"
                ),
                new Palabra(
                        5,
                        "moral",
                        "Disciplina filosófica que estudia el comportamiento humano en " +
                                "cuanto al bien y el mal.",
                        "Del latin moralis=costumbres",
                        "ética",
                        "inmoral"
                ),
                new Palabra(
                        6,
                        "feminismo",
                        "Es un conjunto heterogéneo de movimientos políticos, " +
                                "culturales, económicos y sociales que tiene como objetivo la " +
                                "búsqueda de la igualdad de derechos entre hombres y mujeres, y " +
                                "eliminar la dominación y violencia de los varones sobre las " +
                                "mujeres.",
                        "Del español Femina=mujer y el prefijo -ismo=ideología",
                        "No existe",
                        "No existe"
                ),
                new Palabra(
                        7,
                        "masculinismo",
                        "Es un conjunto polisémico de ideologías y movimientos " +
                                "políticos, culturales y económicos que tienen como objetivo el " +
                                "análisis de la «construcción masculina de la identidad y los " +
                                "problemas de los hombres frente al género».",
                        "Del español Masculino=Hombre y el prefijo -ismo=ideología",
                        "No Existe",
                        "No Existe"
                ),
                new Palabra(
                        8,
                        "igualitarismo",
                        "Es un conjunto de teorías de tipo ético y político que " +
                                "consideran que la igualdad es mejor o justa.",
                        "Del español Igualitario y prefijo -ismo=ideología",
                        "No Existe",
                        "No Existe"
                ),
                new Palabra(
                        9,
                        "machismo",
                        "Es una ideología que engloba el conjunto de actitudes, " +
                                "conductas, prácticas sociales y creencias destinadas a promover " +
                                "la superioridad del hombre sobre la mujer.",
                        "Del español macho y el prefijo -ismo=ideología",
                        "falocracia",
                        "No Existe"
                ),
                new Palabra(
                        10,
                        "hembrismo",
                        "Es una actitud de arrogancia de las mujeres hacia los " +
                                "hombres, o un sesgo discriminatorio claramente favorable a las " +
                                "mujeres en acciones u opiniones.",
                        "Del español hembra y el prefijo -ismo=ideología",
                        "No existe",
                        "No existe"
                ),
                new Palabra(
                        11,
                        "igualdad",
                        "Condición o circunstancia de tener una misma naturaleza, " +
                                "cantidad, calidad, valor o forma, o de compartir alguna " +
                                "cualidad o característica.",
                        "Del latín aequilitas=llano",
                        "paridad",
                        "desigualdad"
                ),
                new Palabra(
                        12,
                        "justicia",
                        "Principio moral que inclina a obrar y juzgar respetando la " +
                                "verdad y dando a cada uno lo que le corresponde.",
                        "Del latín lustitia=derecho",
                        "imparcialidad",
                        "injusticia, parcialidad"
                )
        };
        return listado;
    }
}
