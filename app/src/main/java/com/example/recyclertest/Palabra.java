package com.example.recyclertest;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

public class Palabra {
    int id;
    String palabra;
    String significado;
    String etimologia;
    String sinonimo;
    String antonimo;

    public Palabra(
            int id,
            String palabra,
            String significado,
            String etimologia,
            String sinonimo,
            String antonimo
    ) {
        this.id = id;
        this.palabra = palabra;
        this.significado = significado;
        this.etimologia = etimologia;
        this.sinonimo = sinonimo;
        this.antonimo = antonimo;
    }

    public int getId() {
        return id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getSignificado() {
        return significado;
    }

    public void setSignificado(String significado) {
        this.significado = significado;
    }

    public String getEtimologia() {
        return etimologia;
    }

    public void setEtimologia(String etimologia) {
        this.etimologia = etimologia;
    }

    public String getSinonimo() {
        return sinonimo;
    }

    public void setSinonimo(String sinonimo) {
        this.sinonimo = sinonimo;
    }

    public String getAntonimo() {
        return antonimo;
    }

    public void setAntonimo(String antonimo) {
        this.antonimo = antonimo;
    }

    @NonNull
    @Override
    public String toString() {
        return "Palabra: " + this.palabra;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("palabra", palabra);
        jsonObject.put("significado", significado);
        jsonObject.put("etimologia", etimologia);
        jsonObject.put("sinonimo", sinonimo);
        jsonObject.put("antonimo", antonimo);

        return jsonObject;
    }
}
