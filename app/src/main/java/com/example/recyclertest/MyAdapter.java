package com.example.recyclertest;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private Palabra[] palabras;
    private SelectionTracker tracker;

    public Palabra[] getPalabras() {
        return palabras;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public MyViewHolder(View v) {
            super(v);

            textView = v.findViewById(R.id.adaptador);
        }

        public ItemDetailsLookup.ItemDetails<Long> getItemDetails() {
            return new ItemDetailsLookup.ItemDetails<Long>() {
                @Override
                public int getPosition() {
                    return getAdapterPosition();
                }

                @Nullable
                @Override
                public Long getSelectionKey() {
                    return getItemId();
                }

                public Palabra getPalabra() {
                    return getPalabra();
                }
            };
        }
    }

    public MyAdapter(Palabra[] myDataset) {
        palabras = myDataset;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_adapter_layout, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Palabra palabra = palabras[position];
        holder.textView.setText(palabra.getPalabra());
    }

    @Override
    public int getItemCount() {
        return palabras.length;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.my_adapter_layout;
    }

    @Override
    public long getItemId(int position) {
        return palabras[position].id;
    }

    public Palabra getPalabra(int position) {
        return palabras[position];
    }
}